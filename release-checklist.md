
1. CI pass
2. Coverage good
3. Readme & Cargo.toml updated and ready for release
4. Website
	1. Release post ready
	2. List features included ready
	3. Future improvement list ready
	4. Getting started ready also with updated links
	5. Menu update with the last release references
	7. Old Release page updated
	8. Check & update used by
	9. Website Style & Logos ready
	10. Push the website online !!!
5. Release Tag done
6. Gitlab
	1. Check issue fixes in the release and close them
	2. close the relative milestone
7. Publish release on crates.io !!!
8. Public Broadcast
	1. Mastodon !!!
	2. Reddit ?!
	3. Freshcode !?
9. Update readme and Cargo.toml for next iteration
10. Check and release updated persy_expimp 

