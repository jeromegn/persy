use crate::{
    error::{PERes, TimeoutError},
    id::{index_id_to_segment_id_data, index_id_to_segment_id_meta, IndexId, RecRef, SegmentId},
    persy::PersyImpl,
    transaction::tx_impl::LockedRecord,
};
use std::collections::HashSet;

#[derive(Clone)]
pub(crate) struct IndexDataLocks {
    locked_index_pages: HashSet<RecRef>,
    index_segments: HashSet<SegmentId>,
}
impl IndexDataLocks {
    pub(crate) fn new() -> Self {
        Self {
            locked_index_pages: Default::default(),
            index_segments: Default::default(),
        }
    }

    pub fn unchecked_lock_record(
        &mut self,
        persy: &PersyImpl,
        _segment_id: SegmentId,
        id: &RecRef,
    ) -> Result<bool, TimeoutError> {
        let address = persy.address();
        if !self.locked_index_pages.contains(id) {
            address.acquire_record_lock(id)?;
            self.locked_index_pages.insert(*id);
            Ok(true)
        } else {
            Ok(false)
        }
    }
    pub fn lock_record(
        &mut self,
        persy: &PersyImpl,
        segment_id: SegmentId,
        id: &RecRef,
        version: u16,
    ) -> Result<bool, TimeoutError> {
        let address = persy.address();
        let locked_page = if !self.locked_index_pages.contains(id) {
            address.acquire_record_lock(id)?;
            true
        } else {
            false
        };

        if address
            .check_persistent_records(&[LockedRecord::new(segment_id, *id, version)], true)
            .is_ok()
        {
            if locked_page {
                self.locked_index_pages.insert(*id);
            }
            Ok(true)
        } else {
            if locked_page {
                address.release_record_lock(id)?;
            }
            Ok(false)
        }
    }
    pub fn is_record_locked(&self, id: &RecRef) -> bool {
        self.locked_index_pages.contains(id)
    }
    pub fn is_segment_locked(&self, id: &SegmentId) -> bool {
        self.index_segments.contains(id)
    }
    fn lock_segment(&mut self, id: SegmentId) {
        self.index_segments.insert(id);
    }
    pub fn read_lock_indexes(&mut self, persy_impl: &PersyImpl, to_lock: &[IndexId]) -> Result<(), TimeoutError> {
        let indexes = persy_impl.indexes();
        let address = persy_impl.address();
        //TODO: check in case of failure for double unlocking
        indexes.read_lock_all(to_lock)?;
        for index_id in to_lock {
            let segment_meta = index_id_to_segment_id_meta(index_id);
            address.acquire_segment_read_lock(segment_meta)?;
            self.lock_segment(segment_meta);
            let segment_data = index_id_to_segment_id_data(index_id);
            address.acquire_segment_read_lock(segment_data)?;
            self.lock_segment(segment_data);
        }
        Ok(())
    }
    pub fn unlock_record(&mut self, persy: &PersyImpl, _segment: SegmentId, id: &RecRef) -> PERes<()> {
        let address = persy.address();
        let removed = self.locked_index_pages.remove(id);
        assert!(removed);
        address.release_record_lock(id)?;
        Ok(())
    }
    pub fn extract(mut self, records: &[RecRef]) -> (HashSet<RecRef>, HashSet<SegmentId>) {
        for rec in records {
            self.locked_index_pages.remove(rec);
        }
        (self.locked_index_pages, self.index_segments)
    }
}
