use crate::{
    config::TxStrategy,
    error::{IndexChangeError, PERes},
    id::{index_id_to_segment_id_meta, IndexId, RecRef, SegmentId},
    index::{
        config::{IndexTypeInternal, ValueMode},
        keeper::IndexTransactionKeeper,
        tree::nodes::Value,
    },
    journal::{
        records::{
            Commit, CreateSegment, DeleteRecord, DropSegment, FreedPage, InsertRecord, Metadata, NewSegmentPage,
            PrepareCommit, ReadRecord, Rollback, UpdateRecord,
        },
        Journal, JournalId,
    },
    persy::PersyImpl,
    snapshots::{release_snapshot, SnapshotEntry, SnapshotId},
    transaction::{index_locks::IndexDataLocks, iter::TransactionInsertScanner},
    DropSegmentError, GenericError, PrepareError,
};
use std::{
    collections::{hash_map::Entry, BTreeSet, HashMap, HashSet},
    ops::RangeBounds,
    vec::IntoIter,
};

pub enum SegmentOperation {
    Create(CreateSegment),
    Drop(DropSegment),
}

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub(crate) struct LockedRecord {
    pub(crate) segment_id: SegmentId,
    pub(crate) record_id: RecRef,
    pub(crate) version: u16,
}
impl LockedRecord {
    pub(crate) fn new(segment_id: SegmentId, record_id: RecRef, version: u16) -> Self {
        Self {
            segment_id,
            record_id,
            version,
        }
    }
}

struct Locks {
    records: Vec<LockedRecord>,
    created_updated_segments: Vec<SegmentId>,
    dropped_segments: Vec<SegmentId>,
}

impl Locks {
    fn new(
        records: Vec<LockedRecord>,
        created_updated_segments: Vec<SegmentId>,
        dropped_segments: Vec<SegmentId>,
    ) -> Self {
        Self {
            records,
            created_updated_segments,
            dropped_segments,
        }
    }
    fn extract(self) -> (Vec<RecRef>, Vec<SegmentId>, Vec<SegmentId>) {
        let records = self.records.into_iter().map(|r| r.record_id).collect::<Vec<_>>();
        (records, self.created_updated_segments, self.dropped_segments)
    }
}
#[derive(Clone)]
struct LocksKeeping {
    records: Vec<RecRef>,
    created_updated_segments: Vec<SegmentId>,
    dropped_segments: Vec<SegmentId>,
}
impl LocksKeeping {
    fn new(records: Vec<RecRef>, created_updated_segments: Vec<SegmentId>, dropped_segments: Vec<SegmentId>) -> Self {
        Self {
            records,
            created_updated_segments,
            dropped_segments,
        }
    }
}

#[derive(Clone)]
pub struct PreparedState {
    locked_indexes: Option<Vec<IndexId>>,
    snapshot_id: Option<SnapshotId>,
    data_locks: Option<LocksKeeping>,
}

impl PreparedState {
    fn none() -> Self {
        Self {
            locked_indexes: None,
            snapshot_id: None,
            data_locks: None,
        }
    }

    fn only_indexes(indexes: Vec<IndexId>, index: IndexDataLocks) -> Self {
        let (index_record_locks, segments) = index.extract(&[]);
        let data_locks = Some(LocksKeeping::new(
            index_record_locks.into_iter().collect(),
            segments.into_iter().collect(),
            Vec::new(),
        ));
        Self {
            locked_indexes: Some(indexes),
            snapshot_id: None,
            data_locks,
        }
    }
    fn locked(indexes: Option<Vec<IndexId>>, data: Locks, index: IndexDataLocks) -> Self {
        Self::all_int(None, indexes, data, index)
    }
    fn all(snapshot_id: SnapshotId, indexes: Option<Vec<IndexId>>, data: Locks, index: IndexDataLocks) -> Self {
        Self::all_int(Some(snapshot_id), indexes, data, index)
    }

    fn all_int(
        snapshot_id: Option<SnapshotId>,
        indexes: Option<Vec<IndexId>>,
        data: Locks,
        index: IndexDataLocks,
    ) -> Self {
        let (mut records, mut created_updated_segments, dropped_segments) = data.extract();
        let (index_record_locks, segments) = index.extract(&records);
        created_updated_segments.extend(segments);
        records.extend(index_record_locks.into_iter());
        let data_locks = Some(LocksKeeping::new(records, created_updated_segments, dropped_segments));
        Self {
            locked_indexes: indexes,
            snapshot_id,
            data_locks,
        }
    }
}

pub enum SyncMode {
    Sync,
    BackgroundSync,
}

pub struct TransactionImpl {
    strategy: TxStrategy,
    sync_mode: SyncMode,
    meta_id: Vec<u8>,
    id: JournalId,
    inserted: Vec<InsertRecord>,
    updated: Vec<UpdateRecord>,
    deleted: Vec<DeleteRecord>,
    read: HashMap<RecRef, ReadRecord>,
    segments_operations: Vec<SegmentOperation>,
    segs_created_names: HashSet<String>,
    segs_dropped_names: HashSet<String>,
    segs_created: HashSet<SegmentId>,
    segs_dropped: HashSet<SegmentId>,
    segs_updated: HashSet<SegmentId>,
    freed_pages: Option<Vec<FreedPage>>,
    indexes: Option<IndexTransactionKeeper>,
    segs_new_pages: Vec<NewSegmentPage>,
}

pub enum TxRead {
    Record((u64, u16)),
    Deleted,
    None,
}

pub enum TxSegCheck {
    Created(SegmentId),
    Dropped,
    None,
}

impl TransactionImpl {
    pub fn new(
        journal: &Journal,
        strategy: &TxStrategy,
        sync_mode: SyncMode,
        meta_id: Vec<u8>,
    ) -> PERes<TransactionImpl> {
        let id = journal.start()?;
        journal.log(&Metadata::new(strategy, meta_id.clone()), &id)?;
        Ok(TransactionImpl {
            strategy: strategy.clone(),
            sync_mode,
            meta_id,
            id,
            inserted: Vec::new(),
            updated: Vec::new(),
            deleted: Vec::new(),
            read: HashMap::new(),
            segments_operations: Vec::new(),
            segs_created_names: HashSet::new(),
            segs_dropped_names: HashSet::new(),
            segs_created: HashSet::new(),
            segs_dropped: HashSet::new(),
            segs_updated: HashSet::new(),
            freed_pages: None,
            indexes: Some(IndexTransactionKeeper::new()),
            segs_new_pages: Vec::new(),
        })
    }

    pub fn recover(id: JournalId) -> TransactionImpl {
        TransactionImpl {
            strategy: TxStrategy::LastWin,
            sync_mode: SyncMode::Sync,
            meta_id: Vec::new(),
            id,
            inserted: Vec::new(),
            updated: Vec::new(),
            deleted: Vec::new(),
            read: HashMap::new(),
            segments_operations: Vec::new(),
            segs_created_names: HashSet::new(),
            segs_dropped_names: HashSet::new(),
            segs_created: HashSet::new(),
            segs_dropped: HashSet::new(),
            segs_updated: HashSet::new(),
            freed_pages: None,
            indexes: Some(IndexTransactionKeeper::new()),
            segs_new_pages: Vec::new(),
        }
    }

    pub fn segment_created_in_tx(&self, segment: SegmentId) -> bool {
        self.segs_created.contains(&segment)
    }

    pub fn segment_name_by_id(&self, segment: SegmentId) -> Option<String> {
        for info in &self.segments_operations {
            if let SegmentOperation::Create(ref c) = info {
                if c.segment_id == segment {
                    return Some(c.name.clone());
                }
            }
        }
        None
    }
    pub fn exists_segment(&self, segment: &str) -> TxSegCheck {
        if self.segs_created_names.contains(segment) {
            for a in &self.segments_operations {
                if let SegmentOperation::Create(ref c) = a {
                    if c.name == segment {
                        return TxSegCheck::Created(c.segment_id);
                    }
                }
            }
        } else if self.segs_dropped_names.contains(segment) {
            return TxSegCheck::Dropped;
        }
        TxSegCheck::None
    }

    pub fn add_create_segment(
        &mut self,
        journal: &Journal,
        name: &str,
        segment_id: SegmentId,
        first_page: u64,
    ) -> PERes<()> {
        let create = CreateSegment::new(name, segment_id, first_page);

        journal.log(&create, &self.id)?;
        self.segments_operations.push(SegmentOperation::Create(create));
        self.segs_created.insert(segment_id);
        self.segs_created_names.insert(name.into());
        Ok(())
    }

    pub fn recover_add(&mut self, create: &CreateSegment) {
        self.segments_operations.push(SegmentOperation::Create(create.clone()));
        self.segs_created.insert(create.segment_id);
        self.segs_created_names.insert(create.name.clone());
    }

    pub fn add_drop_segment(
        &mut self,
        journal: &Journal,
        name: &str,
        segment_id: SegmentId,
    ) -> Result<(), DropSegmentError> {
        if self.segs_created_names.contains(name) {
            Err(DropSegmentError::CannotDropSegmentCreatedInTx)
        } else {
            let drop = DropSegment::new(name, segment_id);
            journal.log(&drop, &self.id)?;
            self.segments_operations.push(SegmentOperation::Drop(drop));
            self.segs_dropped.insert(segment_id);
            self.segs_dropped_names.insert(name.into());
            Ok(())
        }
    }

    pub fn recover_drop(&mut self, drop: &DropSegment) {
        self.segments_operations.push(SegmentOperation::Drop(drop.clone()));
        self.segs_dropped.insert(drop.segment_id);
        self.segs_dropped_names.insert(drop.name.clone());
    }

    pub fn add_read(&mut self, journal: &Journal, segment: SegmentId, recref: &RecRef, version: u16) -> PERes<()> {
        if self.strategy == TxStrategy::VersionOnRead {
            let read = ReadRecord::new(segment, recref, version);
            journal.log(&read, &self.id)?;
            self.read.insert(*recref, read);
        }
        Ok(())
    }

    pub fn recover_read(&mut self, read: &ReadRecord) {
        self.read.insert(read.recref, read.clone());
    }

    pub fn add_insert(&mut self, journal: &Journal, segment: SegmentId, rec_ref: &RecRef, record: u64) -> PERes<()> {
        self.segs_updated.insert(segment);
        let insert = InsertRecord::new(segment, rec_ref, record);

        journal.log(&insert, &self.id)?;
        self.inserted.push(insert);
        Ok(())
    }

    pub fn add_new_segment_page(
        &mut self,
        journal: &Journal,
        segment: SegmentId,
        new_page: u64,
        previous_page: u64,
    ) -> PERes<()> {
        let new_page = NewSegmentPage::new(segment, new_page, previous_page);

        journal.log(&new_page, &self.id)?;
        self.segs_new_pages.push(new_page);
        Ok(())
    }

    pub fn recover_insert(&mut self, insert: &InsertRecord) {
        self.segs_updated.insert(insert.segment);
        self.inserted.push(insert.clone());
    }

    pub fn add_update(
        &mut self,
        journal: &Journal,
        segment: SegmentId,
        rec_ref: &RecRef,
        record: u64,
        version: u16,
    ) -> PERes<()> {
        self.segs_updated.insert(segment);
        let update = UpdateRecord::new(segment, rec_ref, record, version);
        journal.log(&update, &self.id)?;
        self.updated.push(update);
        Ok(())
    }

    pub fn recover_update(&mut self, update: &UpdateRecord) {
        self.segs_updated.insert(update.segment);
        self.updated.push(update.clone());
    }

    pub fn add_delete(&mut self, journal: &Journal, segment: SegmentId, rec_ref: &RecRef, version: u16) -> PERes<()> {
        self.segs_updated.insert(segment);
        let delete = DeleteRecord::new(segment, rec_ref, version);
        journal.log(&delete, &self.id)?;
        self.deleted.push(delete);
        Ok(())
    }

    pub fn add_put<K, V>(&mut self, index: IndexId, k: K, v: V)
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
    {
        if let Some(ref mut indexes) = self.indexes {
            indexes.put(index, k, v);
        }
    }

    pub fn add_remove<K, V>(&mut self, index: IndexId, k: K, v: Option<V>)
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
    {
        if let Some(ref mut indexes) = self.indexes {
            indexes.remove(index, k, v);
        }
    }

    pub fn apply_changes<K, V>(
        &self,
        vm: ValueMode,
        index: IndexId,
        index_name: &str,
        k: &K,
        pers: Option<Value<V>>,
    ) -> Result<Option<Value<V>>, IndexChangeError>
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
    {
        if let Some(ref indexes) = self.indexes {
            indexes.apply_changes(index, index_name, vm, k, pers)
        } else {
            Ok(pers)
        }
    }

    pub fn index_range<K, V, R>(&self, index: IndexId, range: R) -> Option<IntoIter<K>>
    where
        K: IndexTypeInternal,
        V: IndexTypeInternal,
        R: RangeBounds<K>,
    {
        if let Some(ind) = &self.indexes {
            ind.range::<K, V, R>(index, range)
        } else {
            None
        }
    }

    pub fn recover_delete(&mut self, delete: &DeleteRecord) {
        self.segs_updated.insert(delete.segment);
        self.deleted.push(delete.clone());
    }

    pub fn scan_insert(&self, seg: SegmentId) -> TransactionInsertScanner {
        TransactionInsertScanner::new(self.inserted.clone(), seg)
    }

    pub fn read(&self, rec_ref: &RecRef) -> TxRead {
        for ele in &self.deleted {
            if ele.recref.page == rec_ref.page && ele.recref.pos == rec_ref.pos {
                return TxRead::Deleted;
            }
        }
        if let Some(ele) = self
            .updated
            .iter()
            .rev()
            .find(|ele| ele.recref.page == rec_ref.page && ele.recref.pos == rec_ref.pos)
        {
            return TxRead::Record((ele.record_page, ele.version));
        }
        for ele in &self.inserted {
            if ele.recref.page == rec_ref.page && ele.recref.pos == rec_ref.pos {
                return TxRead::Record((ele.record_page, 1));
            }
        }
        TxRead::None
    }

    pub fn recover_prepare(&mut self, persy_impl: &PersyImpl) -> Result<PreparedState, PrepareError> {
        let address = persy_impl.address();

        let _ = self.collapse_operations();
        let index_locks = IndexDataLocks::new();
        let locks = self.coll_locks(&index_locks);
        if let Err(x) = address.acquire_locks(&locks.records, &locks.created_updated_segments, &locks.dropped_segments)
        {
            self.recover_rollback(persy_impl)?;
            return Err(x);
        }
        let records = locks.records.clone();
        let crt_upd_segs = locks.created_updated_segments.clone();
        let check_version = self.strategy != TxStrategy::LastWin;
        if let Err(x) = address.check_persistent_records(&records, check_version) {
            //TODO: unlock locked records
            self.recover_rollback(persy_impl)?;
            return Err(x);
        }
        if let Err(x) = address.confirm_allocations(&crt_upd_segs, true) {
            //TODO: unlock locked records
            self.recover_rollback(persy_impl)?;
            return Err(PrepareError::from(x));
        }
        Ok(PreparedState::locked(None, locks, index_locks))
    }

    fn solve_index_locks(&self, index_locks: &IndexDataLocks) -> Vec<LockedRecord> {
        let mut records = HashSet::new();
        for update in &self.updated {
            if index_locks.is_record_locked(&update.recref) {
                records.insert(LockedRecord::new(update.segment, update.recref, update.version));
            }
        }

        for delete in &self.deleted {
            if index_locks.is_record_locked(&delete.recref) {
                records.insert(LockedRecord::new(delete.segment, delete.recref, delete.version));
            }
        }

        records.into_iter().collect()
    }

    pub fn prepare(mut self, persy_impl: &PersyImpl) -> Result<(TransactionImpl, PreparedState), PrepareError> {
        let allocator = persy_impl.allocator();
        let journal = persy_impl.journal();
        let snapshots = persy_impl.snapshots();
        let address = persy_impl.address();

        let ind = self.indexes;
        self.indexes = None;
        let mut locked_indexes = None;
        let mut index_locks = IndexDataLocks::new();
        if let Some(mut ind_change) = ind {
            let changed_indexes = ind_change.changed_indexes();
            for check in changed_indexes {
                let segment_meta = index_id_to_segment_id_meta(&check);
                if self.segs_dropped.contains(&segment_meta) {
                    ind_change.remove_changes(&check);
                }
            }

            let to_lock = ind_change.changed_indexes();
            if let Err(x) = index_locks.read_lock_indexes(&persy_impl, &to_lock) {
                self.rollback_prepared(persy_impl, PreparedState::only_indexes(to_lock, index_locks))?;
                return Err(PrepareError::from(x));
            }
            if let Err(x) = ind_change.apply(&mut index_locks, persy_impl, &mut self) {
                self.rollback_prepared(persy_impl, PreparedState::only_indexes(to_lock, index_locks))?;
                return Err(PrepareError::from(x));
            }
            locked_indexes = Some(to_lock);
        }

        let mut freed_pages = self.collapse_operations();

        let mut locks = self.coll_locks(&index_locks);
        if let Err(x) = address.acquire_locks(&locks.records, &locks.created_updated_segments, &locks.dropped_segments)
        {
            let prepared = if let Some(indexes) = locked_indexes {
                PreparedState::only_indexes(indexes, index_locks)
            } else {
                PreparedState::none()
            };
            self.rollback_prepared(persy_impl, prepared)?;
            return Err(x);
        };
        // This add the already locked index update/deleted record back in list of records for
        // check and track correctly the version for rollback
        locks.records.extend_from_slice(&self.solve_index_locks(&index_locks));
        let check_version = self.strategy != TxStrategy::LastWin;
        let old_records = match address.check_persistent_records(&locks.records, check_version) {
            Ok(old) => old,
            Err(x) => {
                self.rollback_prepared(persy_impl, PreparedState::locked(locked_indexes, locks, index_locks))?;
                return Err(x);
            }
        };
        let segs: Vec<_> = self.segs_updated.iter().copied().collect();
        if let Err(x) = address.confirm_allocations(&segs, false) {
            self.rollback_prepared(persy_impl, PreparedState::locked(locked_indexes, locks, index_locks))?;
            return Err(PrepareError::from(x));
        }

        for dropped_seg in &self.segs_dropped {
            let pages = address.collect_segment_pages(*dropped_seg)?;
            for p in pages.into_iter().map(FreedPage::new) {
                freed_pages.insert(p);
            }
        }
        let mut snapshot_entries = Vec::with_capacity(old_records.len());
        for old_record in &old_records {
            freed_pages.insert(FreedPage::new(old_record.record_page));
            snapshot_entries.push(SnapshotEntry::change(
                &old_record.recref,
                old_record.record_page,
                old_record.version,
            ));
        }
        for insert in &self.inserted {
            snapshot_entries.push(SnapshotEntry::insert(&insert.recref));
        }
        for freed_page in &freed_pages {
            journal.log(freed_page, &self.id)?;
        }
        let mut freed_pages_vec: Vec<_> = freed_pages.into_iter().collect();
        freed_pages_vec.reverse();
        let snapshot_id = snapshots.snapshot(snapshot_entries, freed_pages_vec.clone(), self.id.clone())?;
        self.freed_pages = Some(freed_pages_vec);
        journal.prepare(&PrepareCommit::new(), &self.id)?;
        allocator.flush_free_list()?;
        self.sync(persy_impl, snapshot_id)?;
        Ok((
            self,
            PreparedState::all(snapshot_id, locked_indexes, locks, index_locks),
        ))
    }

    fn sync(&self, persy_impl: &PersyImpl, snapshot_id: SnapshotId) -> PERes<()> {
        persy_impl.transaction_sync(&self.sync_mode, snapshot_id)
    }

    fn collapse_operations(&mut self) -> BTreeSet<FreedPage> {
        let mut pages_to_free = BTreeSet::new();
        let mut inserted_by_id = HashMap::new();
        for insert in self.inserted.drain(..) {
            inserted_by_id.insert(insert.recref, insert);
        }

        let mut updated_by_id = HashMap::new();
        for update in self.updated.drain(..) {
            match updated_by_id.entry(update.recref) {
                Entry::Vacant(e) => {
                    e.insert(update);
                }
                Entry::Occupied(mut e) => {
                    pages_to_free.insert(FreedPage::new(e.get().record_page));
                    e.get_mut().record_page = update.record_page;
                }
            }
        }

        for (k, insert) in &mut inserted_by_id {
            if let Some(update) = updated_by_id.remove(k) {
                pages_to_free.insert(FreedPage::new(insert.record_page));
                insert.record_page = update.record_page;
            }
        }

        let mut i = 0;
        while i != self.deleted.len() {
            if let Some(insert) = inserted_by_id.remove(&self.deleted[i].recref) {
                self.deleted.remove(i);
                pages_to_free.insert(FreedPage::new(insert.record_page));
            } else {
                i += 1;
            }
        }

        for delete in &self.deleted {
            if let Some(update) = updated_by_id.remove(&delete.recref) {
                pages_to_free.insert(FreedPage::new(update.record_page));
            }
        }

        for (_, insert) in inserted_by_id.drain() {
            if self.segs_dropped.contains(&insert.segment) {
                pages_to_free.insert(FreedPage::new(insert.record_page));
            } else {
                self.inserted.push(insert);
            }
        }

        for (_, update) in updated_by_id.drain() {
            if self.segs_dropped.contains(&update.segment) {
                pages_to_free.insert(FreedPage::new(update.record_page));
            } else {
                self.updated.push(update);
            }
        }
        pages_to_free
    }

    fn coll_locks(&self, index_locks: &IndexDataLocks) -> Locks {
        let mut crt_upd_segs = Vec::new();
        for create in &self.segs_created {
            if !&self.segs_dropped.contains(create) && !index_locks.is_segment_locked(create) {
                crt_upd_segs.push(*create);
            }
        }
        for update in &self.segs_updated {
            if !&self.segs_dropped.contains(update) && !index_locks.is_segment_locked(update) {
                crt_upd_segs.push(*update);
            }
        }

        let mut dropped_segs: Vec<_> = self.segs_dropped.iter().copied().collect();
        let mut records = HashSet::new();

        // No need to lock on inserted records the new id unique is managed by the address.
        //
        for update in &self.updated {
            let mut version = update.version;
            // I found values in the read only for VersionOnRead
            if let Some(read_v) = self.read.get(&update.recref) {
                version = read_v.version;
            }
            if !index_locks.is_record_locked(&update.recref) {
                records.insert(LockedRecord::new(update.segment, update.recref, version));
            }
        }

        for delete in &self.deleted {
            let mut version = delete.version;
            // I found values in the read only for VersionOnRead
            if let Some(read_v) = self.read.get(&delete.recref) {
                version = read_v.version;
            }
            if !index_locks.is_record_locked(&delete.recref) {
                records.insert(LockedRecord::new(delete.segment, delete.recref, version));
            }
        }

        for insert in &self.inserted {
            records.remove(&LockedRecord::new(insert.segment, insert.recref, 1));
        }

        let mut records: Vec<LockedRecord> = records.into_iter().collect();
        records.sort_by_key(|l| l.record_id);
        crt_upd_segs.sort();
        dropped_segs.sort();
        Locks::new(records, crt_upd_segs, dropped_segs)
    }

    fn internal_rollback(&self, persy_impl: &PersyImpl) -> PERes<Vec<(SegmentId, u64)>> {
        let address = persy_impl.address();

        let dropped_segs: Vec<_> = self.segs_created.iter().copied().collect();
        let address_to_free = address.rollback(&self.inserted)?;
        let mut free_pages = Vec::new();
        for insert in &self.inserted {
            if dropped_segs.contains(&insert.segment) {
                free_pages.push(insert.record_page);
            }
        }

        for create in &self.segs_created {
            address.drop_temp_segment(*create)?;
        }

        for update in &self.updated {
            if dropped_segs.contains(&update.segment) {
                free_pages.push(update.record_page);
            }
        }
        persy_impl.allocator().free_pages(free_pages.into_iter())?;
        Ok(address_to_free)
    }

    pub fn recover_rollback(&self, persy_impl: &PersyImpl) -> PERes<()> {
        let allocator = persy_impl.allocator();
        let journal = persy_impl.journal();
        let address = persy_impl.address();

        journal.end(&Rollback::new(), &self.id)?;
        let address_to_free = self.internal_rollback(persy_impl)?;
        journal.finished_to_clean(&[self.id.clone()])?;
        if !address_to_free.is_empty() {
            address.clear_empty(&address_to_free)?;
            allocator.free_pages(address_to_free.iter().map(|(_, page)| *page))?;
        }
        Ok(())
    }

    pub fn rollback(&mut self, persy_impl: &PersyImpl) -> PERes<()> {
        let journal = persy_impl.journal();
        journal.end(&Rollback::new(), &self.id)?;
        let address_to_free = self.internal_rollback(persy_impl)?;
        journal.finished_to_clean(&[self.id.clone()])?;
        self.free_address_structures(address_to_free, persy_impl)?;
        Ok(())
    }

    pub fn rollback_prepared(&mut self, persy_impl: &PersyImpl, prepared: PreparedState) -> PERes<()> {
        let indexes = persy_impl.indexes();
        let allocator = persy_impl.allocator();
        let journal = persy_impl.journal();
        let snapshots = persy_impl.snapshots();
        let address = persy_impl.address();

        journal.end(&Rollback::new(), &self.id)?;

        let address_to_free = self.internal_rollback(persy_impl)?;

        if let Some(locks) = &prepared.data_locks {
            address.release_locks(
                locks.records.iter(),
                &locks.created_updated_segments,
                &locks.dropped_segments,
            )?;
        }
        if let Some(il) = &prepared.locked_indexes {
            indexes.read_unlock_all(il)?;
        }
        self.free_address_structures(address_to_free, persy_impl)?;
        if let Some(snapshot_id) = prepared.snapshot_id {
            release_snapshot(snapshot_id, snapshots, allocator, journal)?;
        }
        Ok(())
    }

    pub fn recover_commit(&mut self, persy_impl: &PersyImpl, prepared: PreparedState) -> PERes<()> {
        let allocator = persy_impl.allocator();
        let journal = persy_impl.journal();
        let address = persy_impl.address();

        let address_to_free = self.internal_commit(persy_impl, true, &prepared)?;
        journal.end(&Commit::new(), &self.id)?;
        if !address_to_free.is_empty() {
            address.clear_empty(&address_to_free)?;
            for (_, page) in address_to_free {
                allocator.recover_free(page)?;
            }
        }
        self.recover_cleanup(persy_impl)
    }

    pub(crate) fn remove_free_pages(&mut self, pages: Vec<u64>) {
        if let Some(fp) = &mut self.freed_pages {
            fp.retain(|x| !pages.contains(&x.page));
        }
    }
    pub fn recover_cleanup(&self, persy_impl: &PersyImpl) -> PERes<()> {
        let allocator = persy_impl.allocator();
        let address = persy_impl.address();
        let journal = persy_impl.journal();
        address.recover_segment_operations(&self.segments_operations)?;
        if let Some(ref up_free) = self.freed_pages {
            for to_free in up_free {
                if !journal.is_page_in_start_list(to_free.page) {
                    allocator.recover_free(to_free.page)?;
                }
            }
        }
        persy_impl.allocator().disc_sync()?;
        Ok(())
    }

    fn free_pages_tx(journal: &Journal, pages_to_free: &[(SegmentId, u64)]) -> PERes<(JournalId, Vec<FreedPage>)> {
        let id = journal.start()?;
        let mut freed = Vec::new();
        for (_, page) in pages_to_free {
            let fp = FreedPage::new(*page);
            journal.log(&fp, &id)?;
            freed.push(fp);
        }
        journal.log(&PrepareCommit::new(), &id)?;
        journal.end(&Commit::new(), &id)?;
        Ok((id, freed))
    }

    fn internal_commit(
        &mut self,
        persy_impl: &PersyImpl,
        recover: bool,
        prepared: &PreparedState,
    ) -> PERes<Vec<(SegmentId, u64)>> {
        let indexes = persy_impl.indexes();
        let address = persy_impl.address();

        let pages_to_unlink = address.apply(
            &self.segs_new_pages,
            &self.inserted,
            &self.updated,
            &self.deleted,
            &self.segments_operations,
            recover,
        )?;

        if let Some(locks) = &prepared.data_locks {
            address.release_locks(
                locks.records.iter(),
                &locks.created_updated_segments,
                &locks.dropped_segments,
            )?;
        }

        if let Some(il) = &prepared.locked_indexes {
            indexes.read_unlock_all(il)?;
        }

        Ok(pages_to_unlink)
    }

    fn free_address_structures(&self, address_to_free: Vec<(SegmentId, u64)>, persy_impl: &PersyImpl) -> PERes<()> {
        let allocator = persy_impl.allocator();
        let journal = persy_impl.journal();
        let snapshots = persy_impl.snapshots();
        let address = persy_impl.address();
        if !address_to_free.is_empty() {
            let (tx_id, freed) = TransactionImpl::free_pages_tx(journal, &address_to_free)?;
            address.clear_empty(&address_to_free)?;
            let add_snap_id = snapshots.snapshot(Vec::new(), freed, tx_id)?;
            self.sync(persy_impl, add_snap_id)?;
            // Is a only free page snapshot not a logic snapshot, release straight away, the
            // purpose of this is to delay the free of pages till are not in use anymore
            release_snapshot(add_snap_id, snapshots, allocator, journal)?;
        }
        Ok(())
    }

    pub fn commit(&mut self, persy_impl: &PersyImpl, prepared: PreparedState) -> Result<(), GenericError> {
        let allocator = persy_impl.allocator();
        let journal = persy_impl.journal();

        let address_to_free = self.internal_commit(persy_impl, false, &prepared)?;
        if !self.segments_operations.is_empty() {
            if let Some(snapshot_id) = prepared.snapshot_id {
                self.sync(persy_impl, snapshot_id)?;
            }
        }
        journal.end(&Commit::new(), &self.id)?;
        self.free_address_structures(address_to_free, persy_impl)?;
        if let Some(snapshot_id) = prepared.snapshot_id {
            allocator.to_release_next_sync(snapshot_id);
        }
        Ok(())
    }

    pub fn recover_metadata(&mut self, metadata: &Metadata) {
        self.strategy = metadata.strategy.clone();
        self.meta_id = metadata.meta_id.clone();
    }

    pub fn recover_freed_page(&mut self, freed: &FreedPage) {
        self.freed_pages.get_or_insert(Vec::new()).push(freed.clone());
    }

    pub fn recover_new_segment_page(&mut self, new_page: &NewSegmentPage) {
        self.segs_new_pages.push(new_page.clone());
    }

    pub fn meta_id(&self) -> &Vec<u8> {
        &self.meta_id
    }

    pub fn filter_list<'a>(
        &'a self,
        pers: &'a [(String, SegmentId)],
    ) -> impl Iterator<Item = (&'a str, SegmentId)> + 'a {
        let outer = pers.iter().map(|(name, id)| (name.as_str(), *id));
        let inner = self.segments_operations.iter().filter_map(|seg| {
            if let SegmentOperation::Create(c) = seg {
                Some((c.name.as_str(), c.segment_id))
            } else {
                None
            }
        });

        outer.chain(inner).filter(move |x| !self.segs_dropped.contains(&x.1))
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        id::{RecRef, SegmentId},
        journal::{
            records::{DeleteRecord, FreedPage, InsertRecord, UpdateRecord},
            JournalId,
        },
        transaction::tx_impl::TransactionImpl,
    };

    #[test]
    fn test_scan_insert() {
        let segment_id = SegmentId::new(10);
        let segment_id_other = SegmentId::new(20);
        let mut tx = TransactionImpl::recover(JournalId::new(0, 0));
        tx.inserted.push(InsertRecord::new(segment_id, &RecRef::new(3, 2), 2));
        tx.inserted.push(InsertRecord::new(segment_id, &RecRef::new(4, 2), 2));
        tx.inserted
            .push(InsertRecord::new(segment_id_other, &RecRef::new(0, 1), 3));
        let mut count = 0;
        for x in tx.scan_insert(segment_id) {
            assert_eq!(x.pos, 2);
            count += 1;
        }
        assert_eq!(count, 2);
    }

    #[test]
    fn test_collapse() {
        let segment_id = SegmentId::new(10);
        let segment_id_other = SegmentId::new(20);
        let mut tx = TransactionImpl::recover(JournalId::new(0, 0));
        tx.inserted.push(InsertRecord::new(segment_id, &RecRef::new(3, 1), 1));
        tx.inserted.push(InsertRecord::new(segment_id, &RecRef::new(3, 2), 2));
        tx.inserted
            .push(InsertRecord::new(segment_id_other, &RecRef::new(3, 3), 3));
        tx.inserted
            .push(InsertRecord::new(segment_id_other, &RecRef::new(3, 4), 4));
        tx.updated.push(UpdateRecord::new(segment_id, &RecRef::new(3, 1), 5, 1));
        tx.updated.push(UpdateRecord::new(segment_id, &RecRef::new(3, 1), 6, 1));
        tx.updated.push(UpdateRecord::new(segment_id, &RecRef::new(3, 2), 7, 1));
        tx.updated.push(UpdateRecord::new(segment_id, &RecRef::new(3, 5), 8, 1));
        tx.updated.push(UpdateRecord::new(segment_id, &RecRef::new(3, 5), 9, 1));
        tx.updated
            .push(UpdateRecord::new(segment_id, &RecRef::new(3, 6), 10, 1));
        tx.updated
            .push(UpdateRecord::new(segment_id, &RecRef::new(3, 7), 11, 1));
        tx.deleted.push(DeleteRecord::new(segment_id, &RecRef::new(3, 1), 0));
        tx.deleted.push(DeleteRecord::new(segment_id, &RecRef::new(3, 3), 1));
        tx.deleted.push(DeleteRecord::new(segment_id, &RecRef::new(3, 6), 2));
        tx.deleted.push(DeleteRecord::new(segment_id, &RecRef::new(3, 8), 2));
        let free = tx.collapse_operations();
        assert_eq!(free.len(), 7);
        for e in [1, 2, 3, 5, 6, 8, 10].iter().map(|x| FreedPage::new(*x)) {
            assert!(free.contains(&e));
        }
        assert_eq!(tx.inserted.len(), 2);
        assert_eq!(tx.updated.len(), 2);
        assert_eq!(tx.deleted.len(), 2);
    }
}
