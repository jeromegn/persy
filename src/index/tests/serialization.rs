use crate::id::{PersyId, RecRef};
use crate::index::serialization::{deserialize, serialize};
use crate::index::{
    bytevec::ByteVec,
    config::{IndexTypeInternal, ValueMode},
    string_wrapper::StringWrapper,
    tree::nodes::{compare, Leaf, Node, NodeRef, Nodes, Value},
};
use crate::io::ArcSliceRead;
use rand::random;
use std::{cmp::Ordering, fmt::Debug, sync::Arc};

fn random_pointer() -> NodeRef {
    RecRef::new(random::<u64>(), random::<u32>())
}

#[test]
fn test_serialization_deserialization_nodes() {
    let val1 = random_pointer();
    let val2 = random_pointer();
    let val3 = random_pointer();
    let mut node = Nodes::new_from_split(val1, &[(0, val2)]);
    let pos = node.find(&2).pos;
    node.add(pos, &2, val3.clone());
    let value = serialize::<u8, u8>(&Node::Node(node));
    let vl = value.len();
    let read = deserialize::<u8, u8>(ArcSliceRead::new(Arc::new(value), 0, vl));
    match read {
        Node::Node(n) => {
            assert_eq!(n.keys.len(), 2);
            assert_eq!(n.pointers.len(), 3);
        }
        _ => panic!("expected a node"),
    }
}

fn single_type_leaf_test<K: IndexTypeInternal + Debug, V: IndexTypeInternal + Debug>(key: K, value: V, value1: V) {
    let mut leaf = Leaf::new();
    leaf.insert_or_update(&key, &value, ValueMode::Replace, "deserialization error")
        .expect("insert work");
    let binary = serialize::<K, V>(&Node::Leaf(leaf));
    let bl = binary.len();
    let read = deserialize::<K, V>(ArcSliceRead::new(Arc::new(binary), 0, bl));
    match read {
        Node::Leaf(n) => {
            assert_eq!(n.entries.len(), 1);
            match n.entries[0].value {
                Value::Single(ref iv) => assert_eq!(compare(iv, &value), Ordering::Equal),
                _ => panic!("expected SINGLE"),
            }
        }
        _ => panic!("expected a leaf"),
    }
    let mut leaf_many = Leaf::new();
    leaf_many
        .insert_or_update(&key, &value, ValueMode::Cluster, "deserialization error")
        .expect("insert work");
    leaf_many
        .insert_or_update(&key, &value1, ValueMode::Cluster, "deserialization error")
        .expect("insert work");
    let binary = serialize::<K, V>(&Node::Leaf(leaf_many));
    let bl = binary.len();
    let read = deserialize::<K, V>(ArcSliceRead::new(Arc::new(binary), 0, bl));
    match read {
        Node::Leaf(n) => {
            assert_eq!(n.entries.len(), 1);
            match n.entries[0].value {
                Value::Cluster(ref iv) => {
                    assert_eq!(compare(&iv[0], &value), Ordering::Equal);
                    assert_eq!(compare(&iv[1], &value1), Ordering::Equal);
                }
                _ => panic!("expected CLUSTER"),
            }
        }
        _ => panic!("expected a leaf"),
    }
}

#[test]
fn test_serialization_deserialization_leafs() {
    single_type_leaf_test::<u8, u8>(20, 10, 20);
    single_type_leaf_test::<u16, u16>(20, 10, 20);
    single_type_leaf_test::<u32, u32>(20, 10, 20);
    single_type_leaf_test::<u64, u64>(20, 10, 20);
    single_type_leaf_test::<u128, u128>(20, 10, 20);
    single_type_leaf_test::<i8, i8>(20, 10, 20);
    single_type_leaf_test::<i16, i16>(20, 10, 20);
    single_type_leaf_test::<i32, i32>(20, 10, 20);
    single_type_leaf_test::<i64, i64>(20, 10, 20);
    single_type_leaf_test::<i128, i128>(20, 10, 20);
    single_type_leaf_test::<f32, f32>(20.0, 10.0, 20.0);
    single_type_leaf_test::<f64, f64>(20.0, 10.0, 20.0);
    single_type_leaf_test::<i32, StringWrapper>(
        10,
        StringWrapper::new("a".to_string()),
        StringWrapper::new("b".to_string()),
    );
    single_type_leaf_test::<StringWrapper, i32>(StringWrapper::new("a".to_string()), 10, 20);
    single_type_leaf_test::<StringWrapper, ByteVec>(
        StringWrapper::new("a".to_string()),
        vec![0, 1].into(),
        vec![2, 10].into(),
    );
    single_type_leaf_test::<ByteVec, ByteVec>(vec![11, 12].into(), vec![0, 1].into(), vec![2, 10].into());
    single_type_leaf_test::<StringWrapper, StringWrapper>(
        StringWrapper::new("o".to_string()),
        StringWrapper::new("a".to_string()),
        StringWrapper::new("b".to_string()),
    );
    let id = PersyId(RecRef::new(10, 20));
    let id1 = PersyId(RecRef::new(20, 20));
    let id2 = PersyId(RecRef::new(30, 20));
    single_type_leaf_test::<PersyId, PersyId>(id, id1, id2);
}
