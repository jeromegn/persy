use crate::{
    id::{IndexId, PersyId, RecRef, SegmentId},
    index::{
        bytevec::ByteVec,
        config::IndexTypeInternal,
        keeper::{IndexTransactionKeeper, ValueChange},
        string_wrapper::StringWrapper,
    },
};

use std::fmt::Debug;

fn keeper_test_for_type<K: IndexTypeInternal + PartialEq, V: IndexTypeInternal + Debug + PartialEq>(k: K, dk: K, v: V) {
    let name = IndexId::new(SegmentId::new(30), SegmentId::new(40));
    let mut keeper = IndexTransactionKeeper::new();
    keeper.put(name.clone(), k.clone(), v.clone());
    let ret = keeper.get_changes(name.clone(), &k);
    assert_eq!(ret, Some(vec![ValueChange::Add(v.clone())]));
    keeper.remove(name.clone(), dk.clone(), Some(v.clone()));
    let ret = keeper.get_changes(name, &dk);
    assert_eq!(ret, Some(vec![ValueChange::Remove(Some(v))]));
}

#[test]
fn simple_tx_keeper_test() {
    keeper_test_for_type::<u8, u8>(10, 15, 10);
    keeper_test_for_type::<u16, u16>(10, 15, 10);
    keeper_test_for_type::<u32, u32>(10, 15, 10);
    keeper_test_for_type::<u64, u64>(10, 15, 10);
    keeper_test_for_type::<u128, u128>(10, 15, 10);
    keeper_test_for_type::<i8, i8>(10, 15, 10);
    keeper_test_for_type::<i16, i16>(10, 15, 10);
    keeper_test_for_type::<i32, i32>(10, 15, 10);
    keeper_test_for_type::<i64, i64>(10, 15, 10);
    keeper_test_for_type::<i128, i128>(10, 15, 10);
    keeper_test_for_type::<f32, f32>(20.0, 10.0, 20.0);
    keeper_test_for_type::<f64, f64>(20.0, 10.0, 20.0);
    keeper_test_for_type::<StringWrapper, StringWrapper>(
        StringWrapper::new("a".to_string()),
        StringWrapper::new("b".to_string()),
        StringWrapper::new("a".to_string()),
    );
    keeper_test_for_type::<ByteVec, ByteVec>(vec![0, 1].into(), vec![0, 2].into(), vec![0, 1].into());
    let id = PersyId(RecRef::new(10, 20));
    let id1 = PersyId(RecRef::new(20, 20));
    let id2 = PersyId(RecRef::new(30, 20));
    keeper_test_for_type::<PersyId, PersyId>(id, id1, id2);
}
